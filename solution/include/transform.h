#ifndef TRANSFORM_H
#define TRANSFORM_H
#include "inner_format.h"
#include <inttypes.h>

struct image transform_image(struct image *img);
#endif // TRANSFORM_H
