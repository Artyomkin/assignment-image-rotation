#ifndef INPUT_FORMAT_H
#define INPUT_FORMAT_H
#include "inner_format.h"
#include <stdint.h>
#include <stdio.h>
#pragma pack(push, 1)
struct bmp_header {
  uint16_t bfType;
  uint32_t bfileSize;
  uint32_t bfReserved;
  uint32_t bOffBits;
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t biSizeImage;
  uint32_t biXPelsPerMeter;
  uint32_t biYPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImportant;
};
#pragma pack(pop)

enum reading_status {
  RS_OK,
  RS_OPENING_ERROR,
  RS_INVALID_POINTER,
  RS_EOF,
  RS_ERROR,
  RS_MEMORY_ERROR
};

enum reading_status read_format_image(char *file_path, struct image *img);
void save_format_image(struct image *img, char *file_path);
#endif // INPUT_FORMAT_H
