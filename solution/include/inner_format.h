#ifndef INNER_FORMAT_H
#define INNER_FORMAT_H
#include "stdint.h"
#include <stdbool.h>

struct pixel {
  uint8_t r;
  uint8_t g;
  uint8_t b;
};

struct image {
  uint64_t width;
  uint64_t height;
  struct pixel *data;
};

struct image null_image();

bool is_null_image(struct image *img);

struct image image_init(uint64_t width, uint64_t height);

void image_destroy(struct image *img);

#endif // INNER_FORMAT_H
