#include "transform.h"
#include "inner_format.h"
#include <inttypes.h>
#include <stdio.h>

static void array_rotate(struct pixel *source, struct pixel *target,
                         uint64_t rows, uint64_t cols) {
  if (source == 0 || target == 0) {
    printf("Nullptr in array_rotate");
    return;
  }
  for (uint64_t i = 0; i < rows; i++) {
    for (uint64_t j = 0; j < cols; j++) {
      target[i * cols + j] = source[rows * cols - rows + i - rows * j];
    }
  }
}

struct image transform_image(struct image *img) {
  if (img == 0) {
    printf("Source image is null");
    return null_image();
  }
  uint64_t rows = img->width;
  uint64_t cols = img->height;
  struct image result_img = image_init(cols, rows);
  if (!is_null_image(&result_img)){
    array_rotate(img->data, result_img.data, rows, cols);
    return result_img;
  } else {
      return null_image();
  }

}
