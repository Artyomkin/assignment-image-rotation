
#include "input_format.h"
#include "inner_format.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#define ALIGNMENT 4
#define HEADER_SIZE 54
#define BYTES_PER_PIXEL 3
#define FILE_TYPE 19778
#define INFO_SIZE 40
#define PLANES 1
#define BIT_COUNT 24
#define SAVING_ERROR "Saving image error\n"

FILE *open_format(char *file_path, char *mode) {
  FILE *file = fopen(file_path, mode);
  if (file == 0) {
    printf("Cannot open file\n");
  }
  return file;
}

static enum reading_status read_format_header(FILE *file,
                                              struct bmp_header *header) {
  if (file == NULL) {
    printf("Null file\n");
    return RS_INVALID_POINTER;
  }
  size_t elems_to_read = 1;
  if (fread(header, sizeof(struct bmp_header), elems_to_read, file) <
      elems_to_read) {
    return RS_ERROR;
  };

  if (feof(file)) {
    return RS_EOF;
  } else if (ferror(file)) {
    return RS_ERROR;
  } else {
    return RS_OK;
  }

  return RS_OK;
}

static size_t calculate_padding(uint32_t width) { return width % ALIGNMENT; }

static void close_format(FILE *file) { fclose(file); }

static uint64_t calc_bmp_file_size(uint64_t width, uint64_t height) {
  size_t padding = calculate_padding(width) * height;
  return HEADER_SIZE + BYTES_PER_PIXEL * width * height + padding;
}

static struct bmp_header get_header(uint32_t filesize, uint32_t width,
                                    uint32_t height) {
  struct bmp_header header = {.bfType = FILE_TYPE,
                              .bfileSize = filesize,
                              .bfReserved = 0,
                              .bOffBits = HEADER_SIZE,
                              .biSize = INFO_SIZE,
                              .biWidth = width,
                              .biHeight = height,
                              .biPlanes = PLANES,
                              .biBitCount = BIT_COUNT,
                              .biCompression = 0,
                              .biSizeImage = filesize - HEADER_SIZE,
                              .biXPelsPerMeter = 0,
                              .biYPelsPerMeter = 0,
                              .biClrUsed = 0,
                              .biClrImportant = 0};
  return header;
}

enum reading_status read_format_image(char *file_path, struct image *img) {
  FILE *file = open_format(file_path, "r");
  if (file == NULL) {
    printf("Null file in read_format_image\n");
    return RS_INVALID_POINTER;
  }
  struct bmp_header header;
  enum reading_status header_status = read_format_header(file, &header);
  if (header_status != RS_OK) {
    close_format(file);
    return header_status;
  }
  *img = image_init(header.biWidth, header.biHeight);
  if (img == NULL || img->data == NULL) {
    close_format(file);
    printf("Unable to allocate memory\n");
    return RS_MEMORY_ERROR;
  }

  size_t padding = calculate_padding(header.biWidth);
  fseek(file, header.bOffBits, SEEK_SET);

  for (uint64_t rows = 0; rows < img->height; rows++) {
    for (uint64_t cols = 0; cols < img->width; cols++) {
      size_t bytes_to_read = 1;
      size_t elements_to_read = 1;
      if (fread(&(img->data[rows * img->width + cols].r), bytes_to_read,
                elements_to_read, file) < elements_to_read ||
          fread(&(img->data[rows * img->width + cols].g), bytes_to_read,
                elements_to_read, file) < elements_to_read ||
          fread(&(img->data[rows * img->width + cols].b), bytes_to_read,
                elements_to_read, file) < elements_to_read) {
        printf("Unable to read the image\n");
        close_format(file);
        image_destroy(img);
        return RS_ERROR;
      }
    }
    fseek(file, padding, SEEK_CUR);
  }

  close_format(file);

  return RS_OK;
}

void save_format_image(struct image *img, char *file_path) {
  if (img == NULL) {
    printf("Null image pointer in save_format_image");
    return;
  }
  uint32_t width = img->width;
  uint32_t height = img->height;
  uint32_t filesize = calc_bmp_file_size(width, height);

  struct bmp_header header = get_header(filesize, width, height);

  size_t padding = header.biWidth % ALIGNMENT;
  uint8_t junk = 0;
  FILE *save_file = open_format(file_path, "wb");
  if (save_file == NULL) {
    printf("Unable to open the file");
    return;
  }
  size_t elements_to_write = 1;
  if (fwrite(&header, sizeof(struct bmp_header), elements_to_write, save_file) <
      elements_to_write) {
    printf(SAVING_ERROR);
    close_format(save_file);
    return;
  };
  for (uint64_t i = 0; i < height; i++) {
    for (uint64_t j = 0; j < width; j++) {
      if (fwrite(&img->data[i * img->width + j],
                 sizeof(img->data[i * img->width + j]), elements_to_write,
                 save_file) < elements_to_write) {
        printf(SAVING_ERROR);
        close_format(save_file);
        return;
      };
    }
    for (size_t k = 0; k < padding; k++) {
      if (fwrite(&junk, sizeof(uint8_t), elements_to_write, save_file) <
          elements_to_write) {
        printf(SAVING_ERROR);
        close_format(save_file);
        return;
      };
    }
  }
  close_format(save_file);
}
