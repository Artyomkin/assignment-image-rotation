#include "inner_format.h"
#include "stdint.h"
#include <stdio.h>
#include <stdlib.h>

static struct pixel *pixels_init(uint64_t width, uint64_t height) {
  struct pixel *pixels = malloc(sizeof(struct pixel) * width * height);
  return pixels;
}

static void pixels_destroy(struct pixel *pixels) { free(pixels); }

struct image image_init(uint64_t width, uint64_t height) {
  struct image img = {
      .width = width, .height = height, .data = pixels_init(width, height)};
  if (img.data == 0){
      return null_image();
  }
  return img;
}

void image_destroy(struct image *img) { pixels_destroy(img->data); }

struct image null_image() {
  return (struct image){.width = 0, .height = 0, .data = NULL};
}

bool is_null_image(struct image *img){
    return img->data == 0;
}
