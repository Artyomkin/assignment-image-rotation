#include "inner_format.h"
#include "input_format.h"
#include "transform.h"
#include <inttypes.h>
#include <stdio.h>

int main(int argc, char **argv) {
  if (argc < 3) {
    printf("Недостаточно параметров\n");
  }

  struct image img;
  enum reading_status status = read_format_image(argv[1], &img);
  if (status != RS_OK) {
    printf("ERROR");
    return 1;
  }

  struct image rotated_img = transform_image(&img);

  save_format_image(&rotated_img, argv[2]);

  image_destroy(&img);
  image_destroy(&rotated_img);
}
